package de.hanno.compiler.testpackage;

import de.hanno.compiler.CustomRunnable;

public class TestWithDependency implements CustomRunnable {
	static {
		System.out.println("hello");
	}
	public TestWithDependency() {
		System.out.println("world");
	}

	@Override
	public void run() {
		System.out.println("ran the yyy runnable");
	}
}
