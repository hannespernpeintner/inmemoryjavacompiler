package de.hanno.compiler.testpackage;
import de.hanno.compiler.RuntimeJavaCompiler;

public class TestWithRuntimeDependency implements Runnable {
	static final RuntimeJavaCompiler runtimeJavaCompiler;
	static {
		System.out.println("hello");
		runtimeJavaCompiler = new RuntimeJavaCompiler("blubb/");
	}
	public TestWithRuntimeDependency() {
		System.out.println("world");
	}

	@Override
	public void run() {
		System.out.println("ran the yyy runnable");
	}
}
