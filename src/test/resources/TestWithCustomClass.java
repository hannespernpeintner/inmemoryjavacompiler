package testpackage;

import java.util.concurrent.Callable;
import de.hanno.compiler.CustomClass;

public class Test implements Callable<CustomClass> {
	static {
		System.out.println("hello");
	}
	public Test() {
		System.out.println("world");
	}

	@Override
	public CustomClass call() {
		String myString = "ran the yyy callable";
		System.out.println(myString);
		return new CustomClass();
	}
}
