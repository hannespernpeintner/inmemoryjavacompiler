package testpackage;

import java.util.concurrent.Callable;

public class Test implements Callable<String> {
	static {
		System.out.println("hello");
	}
	public Test() {
		System.out.println("world");
	}

	@Override
	public String call() {
		String myString = "ran the yyy callable";
		System.out.println(myString);
		return myString;
	}
}
