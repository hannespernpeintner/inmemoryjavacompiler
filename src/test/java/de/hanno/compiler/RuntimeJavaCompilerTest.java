package de.hanno.compiler;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class RuntimeJavaCompilerTest {

    static final String workingDir = "target/workingDir/";

    @Test
    public void testCompileFile() throws Exception {
        RuntimeJavaCompiler compiler = new RuntimeJavaCompiler(workingDir);
        Class<?> result = compiler.compile(new File("target/test-classes/Test.java"));

        Assert.assertTrue(result.getName().equals("testpackage.Test"));
    }

    @Test
    public void testCompileSource() throws Exception {
        RuntimeJavaCompiler compiler = new RuntimeJavaCompiler(workingDir);
        File file = new File(getClass().getClassLoader().getResource("TestSource.java").getPath());
        String source = readFile(file);
        Class<?> result = compiler.compile(source);

        Assert.assertTrue(result.getName().equals("testpackage.TestSource"));
    }

    @Test
    public void testCompileFileWithDependencyOnOtherClassFile() throws Exception {
        RuntimeJavaCompiler compiler = new RuntimeJavaCompiler(workingDir);
        Class<?> customRunnableClass = compiler.compile("package de.hanno.compiler; public interface CustomRunnable extends Runnable {}");
        Assert.assertTrue(customRunnableClass.getName().equals("de.hanno.compiler.CustomRunnable"));

        File file = new File(getClass().getClassLoader().getResource("TestWithDependency.java").getPath());
        String source = readFile(file);
        Class<?> result = compiler.compile(source);

        Assert.assertTrue(result.getName().equals("de.hanno.compiler.testpackage.TestWithDependency"));
    }
    @Test
    public void testCompileFileWithDependencyOnRuntimeClass() throws Exception {
        RuntimeJavaCompiler compiler = new RuntimeJavaCompiler(workingDir);

        File file = new File(getClass().getClassLoader().getResource("TestWithRuntimeDependency.java").getPath());
        String source = readFile(file);
        Class<?> result = compiler.compile(source);

        Assert.assertTrue(result.getName().equals("de.hanno.compiler.testpackage.TestWithRuntimeDependency"));
    }


    @Test
    public void testParseFullQualifiedClassNameFromSourceWithDefaultPackage() throws IOException {
        String source = "public class Test implements Runnable {\n" +
                        "\tstatic {\n" +
                        "\t\tSystem.out.println(\"hello\");\n" +
                        "\t}\n" +
                        "\tpublic Test() {\n" +
                        "\t\tSystem.out.println(\"world\");\n" +
                        "\t}\n" +
                        "\n" +
                        "\t@Override\n" +
                        "\tpublic void run() {\n" +
                        "\t\tSystem.out.println(\"ran the yyy runnable\");\n" +
                        "\t}\n" +
                        "}";

        String className = RuntimeJavaCompiler.getFullQualifiedNameFromJavaCode(source);
        Assert.assertEquals("Test", className);
    }

    @Test
    public void testParseFullQualifiedClassNameFromSourceWithPackageNamePresent() throws IOException {
        File file = new File(getClass().getClassLoader().getResource("Test.java").getPath());
        String source = readFile(file);

        String className = RuntimeJavaCompiler.getFullQualifiedNameFromJavaCode(source);
        Assert.assertEquals("testpackage.Test", className);
    }

    @Test
    public void testParsePackageNameFromSourceWithPackageNamePresent() throws IOException {
        File file = new File(getClass().getClassLoader().getResource("Test.java").getPath());
        String source = readFile(file);

        String packageName = RuntimeJavaCompiler.getPackageNameFromJavaCode(source);
        Assert.assertEquals("testpackage", packageName);
    }

    @Test
    public void testParsePackageNameFromSourceWithDefaultPackage() throws IOException {
        String source = "public class Test implements Runnable {\n" +
                        "\tstatic {\n" +
                        "\t\tSystem.out.println(\"hello\");\n" +
                        "\t}\n" +
                        "\tpublic Test() {\n" +
                        "\t\tSystem.out.println(\"world\");\n" +
                        "\t}\n" +
                        "\n" +
                        "\t@Override\n" +
                        "\tpublic void run() {\n" +
                        "\t\tSystem.out.println(\"ran the yyy runnable\");\n" +
                        "\t}\n" +
                        "}";

        String packageName = RuntimeJavaCompiler.getPackageNameFromJavaCode(source);
        Assert.assertEquals("", packageName);
    }

    @Test
    public void testParseClassNameFromSource() {
        String className = RuntimeJavaCompiler.getClassNameFromJavaCode("class Test{");
        Assert.assertEquals("Test", className);

        className = RuntimeJavaCompiler.getClassNameFromJavaCode("class Test {");
        Assert.assertEquals("Test", className);
    }

    @Test(expected = IllegalStateException.class)
    public void testMissingClassKeywordExceptionParseClassNameFromSource() {
        RuntimeJavaCompiler.getClassNameFromJavaCode("clas Test{");
    }
    @Test(expected = IllegalStateException.class)
    public void testMissingWhitespaceExceptionParseClassNameFromSource() {
        RuntimeJavaCompiler.getClassNameFromJavaCode("clas Test{");
    }

    static String readFile(File file) throws IOException {
        StringBuilder fileContents = new StringBuilder((int)file.length());
        Scanner scanner = new Scanner(file);
        String lineSeparator = System.getProperty("line.separator");

        try {
            while(scanner.hasNextLine()) {
                fileContents.append(scanner.nextLine() + lineSeparator);
            }
            return fileContents.toString();
        } finally {
            scanner.close();
        }
    }
}
