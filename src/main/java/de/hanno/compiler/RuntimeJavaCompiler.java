package de.hanno.compiler;

import org.eclipse.jdt.internal.compiler.tool.EclipseCompiler;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.tools.DiagnosticListener;
import javax.tools.FileObject;
import javax.tools.ForwardingJavaFileManager;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;

public class RuntimeJavaCompiler {

    private final String workingDirectory;
    private SpecialClassLoader cl;
    private EclipseCompiler javac;
    private final StandardJavaFileManager standardJavaFileManager;
    private DiagnosticListener diagnosticListener;

    public RuntimeJavaCompiler(String workingDirectory) {
        this.workingDirectory = workingDirectory;
        new File(workingDirectory).mkdirs();
        cl = new SpecialClassLoader();
        javac = new EclipseCompiler();
        standardJavaFileManager = javac.getStandardFileManager(null, null, null);
        diagnosticListener = null;
    }

    public Class<?> compile(String source) throws Exception {
        String packageName = getPackageNameFromJavaCode(source);

        File targetFile = new File(workingDirectory + "/" + packageName.replaceAll("\\.", "/") + "/" + getClassNameFromJavaCode(source) + ".java");
        Writer writer = null;
        try {
            targetFile.getParentFile().mkdirs();
            targetFile.createNewFile();
            writer = new FileWriter(targetFile);
            writer.write(source.toString());
            writer.flush();
        } finally {
            try {
                writer.close();
            } catch (Exception e) {
            }
        }

        return compile(targetFile, getFullQualifiedNameFromJavaCode(source), getPackageNameFromJavaCode(source));
    }

    public Class<?> compile(File sourceFile) throws Exception {
        String source = readFile(sourceFile.getPath(), StandardCharsets.UTF_8);
        return compile(sourceFile, getFullQualifiedNameFromJavaCode(source), getPackageNameFromJavaCode(source));
    }
    public Class<?> compile(File sourceFile, String fullQualifiedClassName, String packageName) throws Exception {
        if(sourceFile == null) {
            throw new IllegalArgumentException("sourceFile should not be empty");
        }

        List<String> options = new ArrayList<>();
        options.add("-classpath");
        options.add(System.getProperty("java.class.path") + ";" + workingDirectory);
        Iterable<? extends JavaFileObject> compilationUnits = standardJavaFileManager.getJavaFileObjectsFromFiles(Arrays.asList(sourceFile));

        JavaCompiler.CompilationTask compile = javac.getTask(new PrintWriter(System.err), standardJavaFileManager, diagnosticListener, options, null, compilationUnits);

        Boolean successFull = compile.call();
        if(!successFull) {
            throw new IllegalStateException("MAKE ME A CUSTOM EXCEPTION!");
        }
        String packagePath = packageName.replaceAll("\\.", "/");
        File packagePathFile = new File(packagePath);
        packagePathFile.mkdirs();
        String classFilePath = sourceFile.getPath().replaceFirst("\\.java", ".class");
        String className = fullQualifiedClassName.substring(fullQualifiedClassName.lastIndexOf('.')+1, fullQualifiedClassName.length());
        String newClassFilePath = packagePath + "/" + className + ".class";
        File target = new File(workingDirectory + "/" + newClassFilePath);
        final Path tmp = target.getParentFile().toPath();
        if (tmp != null) {
            Files.createDirectories(tmp);
        }
        Files.move(new File(classFilePath).toPath(), target.toPath(), StandardCopyOption.REPLACE_EXISTING);

        URLClassLoader classLoader = new URLClassLoader(new URL[]{new File(workingDirectory).toURI().toURL(), sourceFile.getParentFile().toURI().toURL()});
        // Load the class from the classloader by name....
        Class<?> loadedClass = classLoader.loadClass(fullQualifiedClassName);

        return loadedClass;
    }

    private Class<?> compile(String source, String fullQualifiedClassName) throws ClassNotFoundException {
        List options = Collections.emptyList();

        String[] split = fullQualifiedClassName.split("\\.");
        String simpleClassName = split[split.length-1];

        List compilationUnits = Arrays.asList(new MemorySource(simpleClassName, source));
        Iterable classes = null;
        JavaCompiler.CompilationTask compile = javac.getTask(new PrintWriter(System.err), standardJavaFileManager, diagnosticListener, options, classes, compilationUnits);
        boolean res = compile.call();
        return cl.findClass(fullQualifiedClassName);
    }

    class MemorySource extends SimpleJavaFileObject {
        private String src;
        public MemorySource(String name, String src) {
            super(URI.create("file:///" + name + ".java"), Kind.SOURCE);
            this.src = src;
        }
        public CharSequence getCharContent(boolean ignoreEncodingErrors) {
            return src;
        }
        public OutputStream openOutputStream() {
            throw new IllegalStateException();
        }
        public InputStream openInputStream() {
            return new ByteArrayInputStream(src.getBytes());
        }
    }
    class MemoryByteCode extends SimpleJavaFileObject {
        private ByteArrayOutputStream baos;
        public MemoryByteCode(String name) {
            super(URI.create("byte:///" + name + ".class"), Kind.CLASS);
        }
        public CharSequence getCharContent(boolean ignoreEncodingErrors) {
            throw new IllegalStateException();
        }
        public OutputStream openOutputStream() {
            baos = new ByteArrayOutputStream();
            return baos;
        }
        public InputStream openInputStream() {
            throw new IllegalStateException();
        }
        public byte[] getBytes() {
            return baos.toByteArray();
        }
    }
    class SpecialClassLoader extends ClassLoader {
        private Map<String,MemoryByteCode> m = new HashMap<>();

        protected Class<?> findClass(String name) throws ClassNotFoundException {
            MemoryByteCode mbc = m.get(name);
            if (mbc==null){
                mbc = m.get(name.replace(".","/"));
                if (mbc==null){
                    return super.findClass(name);
                }
            }
            return defineClass(name, mbc.getBytes(), 0, mbc.getBytes().length);
        }

        public void addClass(String name, MemoryByteCode mbc) {
            m.put(name, mbc);
        }
    }

    class SpecialJavaFileManager extends ForwardingJavaFileManager {
        private SpecialClassLoader xcl;
        public SpecialJavaFileManager(StandardJavaFileManager sjfm, SpecialClassLoader xcl) {
            super(sjfm);
            this.xcl = xcl;
        }
        public JavaFileObject getJavaFileForOutput(Location location, String name, JavaFileObject.Kind kind, FileObject sibling) throws IOException {
            MemoryByteCode mbc = new MemoryByteCode(name);
            xcl.addClass(name, mbc);
            return mbc;
        }

        public ClassLoader getClassLoader(Location location) {
            return xcl;
        }
    }

    static String readFile(String path, Charset encoding)throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
    public static String getPackageNameFromJavaCode(String javaCode) {
        String packageName = "";

        int indexOfPackageString = javaCode.indexOf("package");
        int index = indexOfPackageString + 8;

        if(indexOfPackageString < 0) {
            return "";
        } else {
            while (index < javaCode.length()) {
                char charAtIndex = javaCode.charAt(index);
                if(';' == charAtIndex) {
                    break;
                } else {
                    packageName += charAtIndex;
                    index++;
                }
            }


            return packageName;
        }
    }

    public static String getFullQualifiedNameFromJavaCode(String javaCode) {
        String packageNameFromJavaCode = getPackageNameFromJavaCode(javaCode);
        boolean defaultPackage = packageNameFromJavaCode == null || packageNameFromJavaCode.isEmpty();
        String classNameFromJavaCode = getClassNameFromJavaCode(javaCode);
        return defaultPackage ? classNameFromJavaCode : packageNameFromJavaCode + "." + classNameFromJavaCode;
    }

    public static String getClassNameFromJavaCode(String javaCode) {
        int firstIndexOfClass = javaCode.indexOf("class");
        if(firstIndexOfClass >= 0) {
            if(javaCode.charAt(firstIndexOfClass + 5) != ' ') {
                throw new IllegalStateException("Not able to parse class name from source code! No whitespace after class name!");
            }
            int index = firstIndexOfClass + 6;
            String className = "";
            while (index < javaCode.length()) {
                char charAtIndex = javaCode.charAt(index);
                if('{' == charAtIndex || ' ' == charAtIndex) {
                    break;
                } else {
                    className += charAtIndex;
                    index++;
                }
            }
            className = className.trim();
            return className;
        } else {

            int firstIndexOfInterface = javaCode.indexOf("interface");
            if(firstIndexOfInterface >= 0) {
                if (javaCode.charAt(firstIndexOfInterface + 9) != ' ') {
                    throw new IllegalStateException("Not able to parse interface name from source code! No whitespace after class name!");
                }
                int index = firstIndexOfInterface + 10;
                String className = "";
                while (index < javaCode.length()) {
                    char charAtIndex = javaCode.charAt(index);
                    if ('{' == charAtIndex || ' ' == charAtIndex) {
                        break;
                    } else {
                        className += charAtIndex;
                        index++;
                    }
                }
                className = className.trim();
                return className;
            } else {
                throw new IllegalStateException("Not able to parse class or interface name from source code! No class keyword present!");
            }
        }
    }

}
